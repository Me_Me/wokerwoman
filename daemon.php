<?php
/**
* 
* Author:       913945036@qq.com
* 
* Created Time: 2021年04月10日 星期六 16时30分51秒
* 
* FileName:     daemon.php
* 
* Description: 守护进程 
**/

function daemonize(){
    //设置权限掩码
    umask(0);
    //更改工作目录到根目录，防止daemon进程默认目录unmount
    chdir("/");
    $i_pid = pcntl_fork();
    
    if($i_pid == 0){
            //设置会话组
            if(posix_setsid() < 0){
                    exit();
            }
            $i_ppid = pcntl_fork();
            if($i_ppid > 0){
                    exit();
            }
            //关闭标准输入
           fclose(STDOUT);
        while(true){
                sleep(1);
        }

    }else if($i_pid > 0){//父进程退出
            exit();
    }
    
}


//daemonize();
